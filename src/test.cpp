/*
 * test.cpp
 *
 *  Created on: 15 Jun 2018
 *      Author: galik
 */

#include "span_allocator.hpp"

#define CATCH_CONFIG_MAIN
#include "catch.hpp"


using namespace galik::memory_utils;

TEST_CASE("Legal parameter values", "[legal]")
{
	SECTION("initialization")
	{
		CHECK_THROWS(span_allocator<int>(-6));
		CHECK_THROWS(span_allocator<int>(0));
		CHECK_NOTHROW(span_allocator<int>(6));
	}

	SECTION("allocation values")
	{
		span_allocator<int> sa(10);

		CHECK_THROWS(sa.allocate(-6));
		CHECK_THROWS(sa.allocate(0));
		CHECK_NOTHROW(sa.allocate(6));
		CHECK_THROWS(sa.allocate(11));
	}
}

TEST_CASE("Allocation behavior", "[allocation]")
{
	SECTION("allocation values")
	{
		{
			span_allocator<int> sa(10);
			CHECK_NOTHROW(sa.allocate(10));
		}
		{
			span_allocator<int> sa(10);
			sa.allocate(10);
			CHECK_NOTHROW(sa.allocate(1));
		}
		{
			span_allocator<int, use_exceptions::off> sa(10);
			sa.allocate(10);
			CHECK_NOTHROW(sa.allocate(1));
		}
		{
			span_allocator<int, use_exceptions::on> sa(10);
			sa.allocate(10);
			CHECK_THROWS(sa.allocate(1));
		}
	}
}

