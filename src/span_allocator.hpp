#ifndef SPAN_ALLOCATOR_HPP
#define SPAN_ALLOCATOR_HPP
/*
 * span_allocator.hpp
 *
 *  Created on: 15 Jun 2018
 *      Author: galik
 */

//
// Copyright (c) 2018 Galik <galik.bool@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do
// so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//

#include <cassert>
#include <iostream> // debugging
#include <optional>
#include <stdexcept>
#include <string>
#include <vector>

#include <gsl/span>

#ifndef IF_CONSTEXPR
#ifdef __cpp_if_constexpr
#define IF_CONSTEXPR(cond) if constexpr(cond)
#else
#define IF_CONSTEXPR(cond) if(cond)
#endif // __cpp_if_constexpr
#endif // IF_CONSTEXPR

#ifndef NDEBUG
#define THROWING_ASSERT(test,msg) do{ if(!(test)) throw std::runtime_error(msg); }while(0)
#else
#define THROWING_ASSERT(test,msg) do{}while(0)
#endif

// standard allocator
#if __cplusplus >= 201703L
#  include <cstddef> // std::byte
#endif


namespace galik {
namespace memory_utils {

#if __cplusplus >= 201703L
	using byte = std::byte;
#else
	enum class byte: unsigned char {};
#endif

enum class use_exceptions: bool {off, on};

template<typename T, use_exceptions exception_state = use_exceptions::off>
class span_allocator
{
	using dist_type = decltype(std::distance((T*)0,(T*)0));

	template<typename TT>
	class vector
	: private std::vector<TT>
	{
	public:
		using std::vector<TT>::iterator;
		using std::vector<TT>::const_iterator;
		using std::vector<TT>::begin;
		using std::vector<TT>::clear;
		using std::vector<TT>::data;
		using std::vector<TT>::end;
		using std::vector<TT>::emplace;
		using std::vector<TT>::emplace_back;
		using std::vector<TT>::empty;
		using std::vector<TT>::insert;
		using std::vector<TT>::push_back;
		using std::vector<TT>::resize;
		using std::vector<TT>::vector;

		gsl::index size() const { return gsl::narrow<gsl::index>(std::vector<TT>::size()); }
	};

	struct entry
	{
		T* ptr;
		bool free;

		entry(T* ptr, bool free): ptr(ptr), free(free) {}
	};

	using value_vec = vector<T>;
	using entry_vec = vector<entry>;

	using entry_iter = typename entry_vec::iterator;
	using entry_citer = typename entry_vec::const_iterator;

public:
	span_allocator(gsl::index n)
	: m_values(n), m_entries(1, {m_values.data(), true}), m_pos(std::begin(m_entries))
	{
		THROWING_ASSERT(n >= 0, "negative size");
		THROWING_ASSERT(n > 0, "zero size");
	}

	span_allocator(span_allocator const&) = delete;
	span_allocator& operator=(span_allocator const&) = delete;

	span_allocator(span_allocator&&) = default;

	/**
	 * Perform *move assignment*. The moved from object is rendered
	 * useless. It will ever after fail to allocate. If NDEBUG is undefined the
	 * moved from object will fail its assert() tests when calling allocate()
	 * otherwise it will return an error/throw an exception.
	 * @param other The object to move from.
	 * @return *this
	 */
	span_allocator& operator=(span_allocator&& other)
	{
		m_values = std::move(other.m_values);
		m_entries = std::move(other.m_entries);
		m_pos = std::move(other.m_pos);
		// makes the moved from object always fail to allocate
		other.m_entries.clear();
		return *this;
	}

	std::optional<gsl::span<T>> allocate(gsl::index n)
	{
		// this should NEVER be broken unless the object has been moved from
		THROWING_ASSERT(!m_entries.empty(), "bad state");
		THROWING_ASSERT(n >= 0, "negative size");
		THROWING_ASSERT(n > 0, "zero size");
		THROWING_ASSERT(n <= m_values.size(), "requested size larger that entire pool");

		auto const end = std::end(m_entries);
		auto const end_pos = m_pos;

		for(; m_pos != end; ++m_pos)
			if(m_pos->free && entry_size(m_pos) >= n)
				break;

		if(m_pos == end)
		{
			for(m_pos = std::begin(m_entries); m_pos != end_pos; ++m_pos)
				if(m_pos->free && entry_size(m_pos) >= n)
					break;

			if(m_pos == end_pos)
			{
				IF_CONSTEXPR(exception_state == use_exceptions::on)
					throw std::runtime_error("unable to allocate " + std::to_string(n) + " elements.");
				else
				 return {};
			}
		}

		m_pos->free = false;
		auto sp = gsl::make_span(m_pos->ptr, n);

		if(entry_size(m_pos) > n)
			m_pos = m_entries.insert(std::next(m_pos), {m_pos->ptr + n, true});

		return sp;
	}

	bool deallocate(gsl::span<T> sp)
	{
		auto found = std::find_if(std::begin(m_entries), std::end(m_entries), [&](entry e){
			return sp.data() == e.ptr;
		});

		if(found == std::end(m_entries) || found->free)
		{
			IF_CONSTEXPR(exception_state == use_exceptions::on)
				throw std::runtime_error("attempt to deallocate non-allocated");
			return false;
		}

		found->free = true;

		if(found != std::begin(m_entries) && std::prev(found)->free)
			found = std::prev(found);

		auto end_found = std::find_if(found, std::end(m_entries), [](entry e){
			return !e.free;
		});

		if(end_found != found)
			m_entries.erase(std::next(found), end_found);

		return true;
	}

//	void throw_exception_on_failure() { exceptions = true; }
//	void return_error_on_failure() { exceptions = false; }

	void dump()
	{
		for(auto e: m_entries)
		{
			std::cout << (e.free ? "F":"U") << ": " << std::distance(m_values.data(), e.ptr) << '\n';
		}
	}

private:
	dist_type entry_size(entry_iter from)
	{
		auto to = std::next(from);

		if(to == std::end(m_entries))
			return std::distance(from->ptr, m_values.data() + m_values.size());
		return std::distance(from->ptr, to->ptr);
	}

	value_vec m_values;
	entry_vec m_entries;
	entry_iter m_pos;
};

template<typename T>
using throwing_span_allocator = span_allocator<T, use_exceptions::on>;

template<typename T>
class span_std_allocator
{
public:
	using value_type         = T;
	using pointer            = typename std::allocator_traits<T>::pointer;
	using const_pointer      = typename std::allocator_traits<T>::const_pointer;
	using void_pointer       = typename std::allocator_traits<T>::void_pointer;
	using const_void_pointer = typename std::allocator_traits<T>::const_void_pointer;
	using size_type          = typename std::allocator_traits<T>::size_type;
	using difference_type    = typename std::allocator_traits<T>::difference_type;
//	using rebind             = typename std::allocator_traits<T>::rebind;

	template<typename U>
	struct rebind { using other = span_std_allocator<U>; };

	span_std_allocator(std::size_t n): sa(gsl::index(n)) {}


	pointer allocate(size_type n)
	{
		auto n_bytes = (n + 1) * sizeof(value_type);
		auto sp = sa.allocate(n_bytes);
		spans.insert(std::lower_bound(std::begin(spans), std::end(spans), sp), sp);
		return static_cast<pointer>(sp.data());
	}

private:
	span_allocator<byte> sa;
	std::vector<gsl::span<byte>> spans;
};

}  // namespace memory_utils
}  // namespace galik

#endif // SPAN_ALLOCATOR_HPP
