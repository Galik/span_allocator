
MD := mkdir -p

SRC := src
BIN := bin

CXXFLAGS := -std=c++17 -MMD -MP -pedantic-errors
CPPFLAGS := -Ims-gsl-v1.0.0

TEST_SRCS := $(wildcard $(SRC)/test*.cpp)
TEST_PRGS := $(patsubst $(SRC)%.cpp,$(BIN)%,$(TEST_SRCS))
TEST_DEPS := $(patsubst $(SRC)%.cpp,$(BIN)%.d,$(TEST_SRCS))

all: prereqs $(TEST_PRGS)

$(BIN)/%: $(SRC)/%.cpp
	$(CXX) $(CXXFLAGS) $(CPPFLAGS) -o $@ $<

-include $(TEST_DEPS)

clean: 
	$(RM) $(TEST_PRGS)
	
prereqs:
	$(MD) $(BIN)

.PHONY: all clean prereqs
