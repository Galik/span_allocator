
#Span Allocator

This is a single header only class that grabs an internal pool of memory
and hands out pieces of it as `gsl::span` objects.

Requires [GSL span](https://github.com/Microsoft/GSL/blob/v1.0.0/include/gsl/span) v1.0

